#include <iostream>
using namespace std;

class Solution {
public:
    /*
     * @param a: The first integer
     * @param b: The second integer
     * @return: The sum of a and b
     */
    int aplusb(int a, int b)
    {
        // write your code here, try to do it without arithmetic operators.
        int sum = a;
        int carry = b;
        int temp;
        
        while(carry)
        {
            temp = sum;
            sum = sum^carry;
            carry = (temp&carry)<<1;
        }
        
        return sum;
    }
    
    int aminusb(int a, int b)
    {
        return aplusb(a, aplusb(~b, 1));
    }
    
    int amultipleb(int a, int b)
    {
        if (a < 0)
        {
            a = aplusb(~a, 1);
        }
        if (b < 0)
        {
            b = aplusb(~b, 1);
        }
        
        // cout << a << " " <<b<<endl;
        int multiplier = a;
        int multiplicant = b;
        int product = 0;
        
        while (multiplier)
        {
            if (multiplier & 1)
            {
                product = aplusb(product, multiplicant);
            }
            
            multiplier = multiplier>>1;
            multiplicant = multiplicant<<1;
        }
        
        if ((a^b) < 0)
        {
            product = aplusb(~product, 1);
        }
        
        return product;
    }
};

int main()
{
    int a,b;
    Solution S;
    cin>>a;
    cin>>b;
    //cout<<S.aplusb(a,b)<<endl;
    //cout<<S.aminusb(a,b)<<endl;
    cout<<S.amultipleb(a, b)<<endl;
}