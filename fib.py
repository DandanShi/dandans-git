# Uses python3
def calc_fib(n):
    
    if (n <= 1):
        return n
    
    # else n >= 2
    arr = []
    arr.append(0)
    arr.append(1)
    for i in range(2,n+1):
    	fib_1 = arr[i-1]
    	fib_2 = arr[i-2]
    	fib_i = fib_1 + fib_2
    	#print fib_i
    	arr.append(fib_i)

    return arr[n]

n = int(input())
fib_n = calc_fib(n)
print "fib of",n, "is:",fib_n
